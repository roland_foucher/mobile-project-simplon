// A Widget that extracts the necessary arguments from
// the ModalRoute.
import 'package:flutter/material.dart';
import 'package:flutter_application_1/helpers/color_helper.dart';
import 'package:flutter_application_1/models/exchange_Item.dart';
import 'package:flutter_application_1/widgets/button_validator_widget.dart';
import 'package:flutter_application_1/widgets/item_description_widget.dart';

class ItemDescriptionPage extends StatelessWidget {
  const ItemDescriptionPage({super.key});

  static const routeName = '/itemDescription';

  @override
  Widget build(BuildContext context) {
    // Extract the arguments from the current ModalRoute
    // settings and cast them as ScreenArguments.
    final item = ModalRoute.of(context)!.settings.arguments as ExchangeItem;

    return Theme(
      data: ThemeData.dark(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            item.title,
            style: const TextStyle(color: Colors.black),
          ),
          backgroundColor: baseColor,
        ),
        body: Column(
          children: [
            Expanded(
              // prend toute la place jusqu'au suivant element
              child: SingleChildScrollView(
                // la première partie est scrollbale
                child: ItemDescriptionWidget(item: item),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: ButtonValidatorWidget(
                buttonText: 'Intéressé(e) ?',
                onPressed: () => {},
              ),
            )
          ],
        ),
      ),
    );
  }
}
