import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/helpers/color_helper.dart';
import 'package:flutter_application_1/widgets/image_picker_widget.dart';
import 'package:flutter_application_1/widgets/new_item_form.dart';

class NewItemPage extends StatelessWidget {
  static const routeName = '/addItem';
  File? file;

  NewItemPage({super.key, this.file});

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData.light(),
        child: Scaffold(
            appBar: AppBar(
              title: const Text(
                "Créer un item",
                style: TextStyle(color: Colors.black),
              ),
              backgroundColor: baseColor,
            ),
            body: file == null
                ? const ImagePickerWidget()
                : NewItemForm(file: file)));
  }
}
