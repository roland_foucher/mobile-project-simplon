import 'package:flutter/material.dart';
import 'package:flutter_application_1/helpers/color_helper.dart';
import 'package:flutter_application_1/models/exchange_Item.dart';
import 'package:flutter_application_1/pages/item_description_page.dart';
import 'package:flutter_application_1/services/exchange_sevice.dart';
import 'package:flutter_application_1/widgets/exchange_item_widget.dart';
import 'package:flutterfire_ui/firestore.dart';

class ExchangePage extends StatefulWidget {
  const ExchangePage({super.key});

  @override
  State<ExchangePage> createState() => _ExchangePageState();
}

class _ExchangePageState extends State<ExchangePage> {
  @override
  Widget build(BuildContext context) {
    return FirestoreQueryBuilder<Map<String, dynamic>>(
        query: ExchangeService.getItems(),
        builder: (context, snapshot, _) {
          // Waiting
          if (snapshot.isFetching) {
            return Center(child: CircularProgressIndicator(color: baseColor));
          }

          // Il y a des données
          if (snapshot.hasData) {
            return ListView.separated(
              padding: const EdgeInsets.all(8),
              itemCount: snapshot.docs.length,
              itemBuilder: (BuildContext context, int index) {
                if (snapshot.hasMore && index + 1 == snapshot.docs.length) {
                  snapshot.fetchMore();
                }
                final ExchangeItem item =
                    ExchangeService.convertDocSnapshotToObject(
                  snapshot.docs[index],
                );
                return ExchangeItemWidget(
                  item: item,
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      ItemDescriptionPage.routeName,
                      arguments: item,
                    );
                  },
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            );
          }

          // pas de données
          if (!snapshot.hasData) {
            return const Text("Rien à échanger pour le moment");
          }

          // erreurs
          return Row(
            children: [
              const Icon(Icons.error),
              Column(
                children: [
                  const Text("Une erreur est survenue"),
                  if (snapshot.hasError) Text(snapshot.error.toString())
                ],
              )
            ],
          );
        });
  }
}
