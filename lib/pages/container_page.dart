import 'dart:io';

import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:community_material_icon/community_material_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/helpers/color_helper.dart';
import 'package:flutter_application_1/pages/exchange_page.dart';
import 'package:flutter_application_1/pages/gift_page.dart';
import 'package:flutter_application_1/pages/new_item_page.dart';

class MyContainerPage extends StatefulWidget {
  const MyContainerPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyContainerPage> createState() => _MyContainerPageState();
}

class _MyContainerPageState extends State<MyContainerPage>
    with TickerProviderStateMixin {
  final autoSizeGroup = AutoSizeGroup();
  var _bottomNavIndex = 0; //default index of a first screen

  final iconList = <IconData>[
    Icons.shopping_bag,
    CommunityMaterialIcons.hand_heart,
  ];

  final iconText = <String>[
    "Echanges",
    "Dons",
  ];

  final pageList = <Widget>[const ExchangePage(), const GiftPage()];

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData.dark(),
      child: Scaffold(
        extendBody: false,
        appBar: AppBar(
          title: Text(
            widget.title,
            style: const TextStyle(color: Colors.black),
          ),
          backgroundColor: baseColor,

          // centre le titre sur android seulement
          centerTitle: Platform.isAndroid && true,
        ),
        body: pageList[_bottomNavIndex],
        floatingActionButton: FloatingActionButton(
          backgroundColor: baseColor,
          child: Icon(
            Icons.add_circle_outline,
            color: HexColor('#373A36'),
          ),
          onPressed: () {
            Navigator.pushNamed(
              context,
              NewItemPage.routeName,
            );
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: getBottomNavigationBar(),
      ),
    );
  }

  // build de la  navigation bar bottom
  AnimatedBottomNavigationBar getBottomNavigationBar() {
    return AnimatedBottomNavigationBar.builder(
      itemCount: iconList.length,
      tabBuilder: (int index, bool isActive) {
        final color = isActive ? baseColor : Colors.white;
        return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              iconList[index],
              size: 24,
              color: color,
            ),
            const SizedBox(height: 4),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: AutoSizeText(
                iconText[index],
                maxLines: 1,
                style: TextStyle(color: color),
                group: autoSizeGroup,
              ),
            )
          ],
        );
      },
      backgroundColor: HexColor('#373A36'),
      activeIndex: _bottomNavIndex,
      splashColor: baseColor,
      splashSpeedInMilliseconds: 300,
      gapLocation: GapLocation.center,
      leftCornerRadius: 32,
      rightCornerRadius: 32,
      onTap: (index) => setState(() => _bottomNavIndex = index),
    );
  }
}
