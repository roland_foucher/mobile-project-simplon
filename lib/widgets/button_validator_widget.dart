import 'package:flutter/material.dart';
import 'package:flutter_application_1/helpers/color_helper.dart';

class ButtonValidatorWidget extends StatelessWidget {
  final void Function()? onPressed;
  final String buttonText;

  const ButtonValidatorWidget(
      {super.key, required this.buttonText, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: TextButton.styleFrom(
          backgroundColor: baseColor,
          padding:
              const EdgeInsets.only(left: 80, right: 80, top: 15, bottom: 15)),
      onPressed: onPressed,
      child: Text(
        buttonText,
        style: const TextStyle(
          fontSize: 20,
        ),
      ),
    );
  }
}
