import 'package:flutter/material.dart';
import 'package:flutter_application_1/models/exchange_Item.dart';

class ItemDescriptionWidget extends StatelessWidget {
  final ExchangeItem item;

  const ItemDescriptionWidget({super.key, required this.item});

  @override
  Column build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
          child: SizedBox(
            height: 350,
            child: item.picture != null
                ? Image(image: NetworkImage(item.picture!))
                : const Icon(Icons.image),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
          child: Text(
            item.subTitle != null ? item.subTitle! : "",
            style: const TextStyle(
              fontSize: 25,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(item.description),
        )
      ],
    );
  }
}
