import 'package:flutter/material.dart';
import 'package:flutter_application_1/helpers/Color_helper.dart';
import 'package:flutter_application_1/models/exchange_Item.dart';

class ExchangeItemWidget extends StatelessWidget {
  final ExchangeItem item;
  final void Function()? onTap;

  const ExchangeItemWidget({super.key, required this.item, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        splashColor: baseColor,
        onTap: onTap,
        child: ListTile(
          leading: getLeading(),
          title: Text("Titre : ${item.title}"),
          subtitle: item.subTitle != null ? Text(item.subTitle!) : null,
          trailing: const Icon(
            Icons.chevron_right,
          ),
        ));
  }

  Widget getLeading() {
    return SizedBox(
      height: 50,
      width: 50,
      child: item.picture != null
          ? Image(image: NetworkImage(item.picture!))
          : const Icon(Icons.image),
    );
  }
}
