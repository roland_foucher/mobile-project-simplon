import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/models/exchange_Item.dart';
import 'package:flutter_application_1/pages/exchange_page.dart';
import 'package:flutter_application_1/services/exchange_sevice.dart';
import 'package:flutter_application_1/theme.dart';
import 'package:flutter_application_1/widgets/button_validator_widget.dart';

class NewItemForm extends StatefulWidget {
  File? file;
  NewItemForm({super.key, this.file});

  @override
  NewItemFormState createState() {
    return NewItemFormState(file: file);
  }
}

class NewItemFormState extends State<NewItemForm> {
  final _formKey = GlobalKey<FormState>();
  final _titleController = TextEditingController();
  final _subtitleController = TextEditingController();
  final _descriptionController = TextEditingController();
  File? file;

  @override
  void dispose() {
    _titleController.dispose();
    _subtitleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  NewItemFormState({this.file});

  String? _validateInputShortString(String value) {
    // The validator receives the text that the user has entered.
    if (value.isEmpty) {
      return "Merci d'entrée une valeur";
    }
    if (value.length > 30) {
      return "Merci de renseigner un titre plus court";
    }
    return null;
  }

  String? _validateInputString(String value) {
    // The validator receives the text that the user has entered.
    if (value.isEmpty) {
      return "Merci d'entrée une valeur";
    }
    return null;
  }

  void saveItem() {
    if (_formKey.currentState!.validate()) {
      ExchangeItem item = ExchangeItem(
        title: _titleController.text,
        subTitle: _subtitleController.text,
        description: _descriptionController.text,
      );

      ExchangeService.saveItem(item, file).then(
        (item) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text("l'item ${item?.title} à bien été enregistré !"),
            ),
          );
          Navigator.popUntil(context, (route) => route.isFirst);
        },
      ).catchError(
        (error) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("une erreur est survenue ! : $error"),
          ));
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData.light(),
      child: Form(
        key: _formKey,
        // ignore: unnecessary_new
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ListView(
            children: <Widget>[
              TextFormField(
                controller: _titleController,
                style: const TextStyle(color: Colors.white),
                decoration: const InputDecoration(
                  hintText: 'Titre',
                  labelText: 'Titre de votre publication',
                ),
                validator: (value) => _validateInputShortString(value!),
              ),
              const SizedBox(height: 10),
              TextFormField(
                controller: _subtitleController,
                style: const TextStyle(color: Colors.black),
                decoration: const InputDecoration(
                  hintText: 'Sous-Titre',
                  labelText: 'Une courte déscription (optionnelle)',
                ),
                validator: (value) => _validateInputShortString(value!),
              ),
              const SizedBox(height: 10),
              TextFormField(
                controller: _descriptionController,
                style: const TextStyle(color: Colors.black),
                keyboardType: TextInputType.multiline,
                maxLines: null,
                decoration: const InputDecoration(
                  hintText: 'Description',
                  labelText: 'Décrivez votre cadeau !',
                ),
                validator: (value) => _validateInputString(value!),
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: ButtonValidatorWidget(
                  buttonText: 'Valider',
                  onPressed: () => {saveItem()},
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
