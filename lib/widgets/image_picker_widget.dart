import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/helpers/adaptive_action_sheet.dart';
import 'package:flutter_application_1/pages/new_item_page.dart';
import 'package:flutter_application_1/widgets/button_validator_widget.dart';
import 'package:flutter_application_1/widgets/new_item_form.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerWidget extends StatefulWidget {
  const ImagePickerWidget({super.key});

  @override
  State<ImagePickerWidget> createState() => _ImagePickerWidgetState();
}

class _ImagePickerWidgetState extends State<ImagePickerWidget> {
  File? imgFile;
  final imgPicker = ImagePicker();

  final actionSheetChoices = const TextStyle(
    color: Colors.blue,
    fontFamily: 'Arial',
    fontSize: 17,
  );
  final actionSheetCancel = const TextStyle(
    color: Colors.blue,
    fontFamily: 'Arial',
    fontSize: 17,
    fontWeight: FontWeight.bold,
  );
  final actionSheetChoicesRed = const TextStyle(
    color: Colors.red,
    fontFamily: 'Arial',
    fontSize: 17,
  );

  Future<void> routeToForm(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return NewItemForm(file: imgFile);
      },
    );
  }

  Future<void> showOptionsDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("Options"),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  GestureDetector(
                    child: const Text("Prendre une photo"),
                    onTap: () {
                      openCamera();
                    },
                  ),
                  const Padding(padding: EdgeInsets.all(10)),
                  GestureDetector(
                    child: const Text("Récupérer une image dans ma gallerie"),
                    onTap: () {
                      openGallery();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  void openCamera() async {
    var imgCamera = await imgPicker.pickImage(source: ImageSource.camera);
    setState(() {
      if (imgCamera?.path != null) {
        imgFile = File(imgCamera!.path);
      }
    });
    Navigator.of(context).pop();
  }

  void openGallery() async {
    var imgGallery = await imgPicker.pickImage(source: ImageSource.gallery);
    setState(() {
      if (imgGallery?.path != null) {
        imgFile = File(imgGallery!.path);
      }
    });
    Navigator.of(context).pop();
  }

  Widget displayImage() {
    if (imgFile == null) {
      return const Text("Merci de séléctionner une image");
    } else {
      return ListBody(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.all(5),
                child: ClipOval(
                  child: Material(
                    color: Colors.red,
                    child: InkWell(
                      splashColor: Colors.white,
                      onTap: _onTapTrash,
                      child: const SizedBox(
                        width: 50,
                        height: 50,
                        child: Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
          Image.file(imgFile!, width: 350, height: 350),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          displayImage(),
          const SizedBox(height: 30),
          ButtonValidatorWidget(
            buttonText:
                imgFile == null ? "Ajouter une photo" : "valider la photo",
            onPressed: () => imgFile == null
                ? showOptionsDialog(context)
                : Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => NewItemPage(file: imgFile!),
                    ),
                  ),
          )
        ],
      ),
    );
  }

  _onTapTrash() {
    showAdaptiveActionSheet(
      context: context,
      actions: <BottomSheetAction>[
        BottomSheetAction(
          title: Text(
            'Supprimer la photo',
            style: actionSheetChoicesRed,
          ),
          onPressed: _deleteImage,
        ),
      ],
      cancelAction: CancelAction(
        title: Text(
          'Annuler',
          style: actionSheetCancel,
        ),
      ),
    );
  }

  _deleteImage() {
    setState(() {
      imgFile = null;
    });
  }
}
