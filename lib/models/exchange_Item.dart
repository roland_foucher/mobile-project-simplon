import 'dart:io';

class ExchangeItem {
  String? id;
  String title;
  String? subTitle;
  String? picture;
  String description;
  String? compensation;

  ExchangeItem({
    this.id,
    required this.title,
    this.picture,
    this.subTitle,
    required this.description,
    this.compensation,
  });
}
