import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/pages/container_page.dart';
import 'package:flutter_application_1/pages/exchange_page.dart';
import 'package:flutter_application_1/pages/item_description_page.dart';
import 'package:flutter_application_1/pages/new_item_page.dart';
import 'package:flutter_application_1/theme.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Gift App',
      theme: MyTheme.getMyTheme(),
      home: const MyContainerPage(title: 'Gift app'),
      // les routes doivent être indiquées ici
      routes: {
        ItemDescriptionPage.routeName: (context) => const ItemDescriptionPage(),
        NewItemPage.routeName: (
          context,
        ) =>
            NewItemPage(),
      },
    );
  }
}
