import 'package:flutter/material.dart';
import 'package:flutter_application_1/helpers/Color_helper.dart';

class MyTheme {
  static getMyTheme() {
    return ThemeData(
      brightness: Brightness.light,
      primarySwatch: Colors.blue,
      scaffoldBackgroundColor: Colors.white,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      appBarTheme: AppBarTheme(
        backgroundColor: baseColor,
        // Couleur de l'icône "retour"
        iconTheme: const IconThemeData(color: Colors.black),
        // Couleur des icônes d'actions situés à droite
        actionsIconTheme: const IconThemeData(color: Colors.black),
        centerTitle: true,
      ),
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: const TextStyle(color: Colors.white),
        hintStyle: const TextStyle(color: Colors.grey),
        floatingLabelStyle: const TextStyle(color: Colors.white),
        border: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(8),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(8),
        ),
      ),
    );
  }
}
