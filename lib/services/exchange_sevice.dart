import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_application_1/models/exchange_Item.dart';

class ExchangeService {
  ///Transforme un document snapshot en objet ExchangeItem
  static ExchangeItem convertDocSnapshotToObject(
    DocumentSnapshot<Map<String, dynamic>> docSnapshot,
  ) {
    final exchangeItemData = docSnapshot.data()!;
    debugPrint(exchangeItemData.toString());
    return ExchangeItem(
      id: docSnapshot.id,
      title: exchangeItemData['title'],
      description: exchangeItemData['description'],
      subTitle: exchangeItemData['subTitle'],
      picture: exchangeItemData['imageUrl'],
    );
  }

  ///Récupère les items à échanger
  static Query<Map<String, dynamic>> getItems() {
    return FirebaseFirestore.instance.collection("items").orderBy("title");
  }

  static Future<ExchangeItem?> saveItem(
      ExchangeItem item, File? imageFile) async {
    await FirebaseFirestore.instance.collection("items").add({
      'title': item.title,
      'description': item.description,
      'subTitle': item.subTitle,
      //'imageUrl': item.picture
    }).then((item) async {
      if (imageFile != null) {
        FirebaseStorage.instance
            .ref("/photos/${item.id}")
            .putFile(imageFile)
            .then(
          (takeSnapShot) async {
            String downloadURL = await takeSnapShot.ref.getDownloadURL();
            FirebaseFirestore.instance.collection("items").doc(item.id).set(
              {'imageUrl': downloadURL},
              SetOptions(merge: true),
            );
          },
        );
      }
      DocumentSnapshot<Map<String, dynamic>> snapshot = await item.get();
      return convertDocSnapshotToObject(snapshot);
    });
    return null;
  }
}
